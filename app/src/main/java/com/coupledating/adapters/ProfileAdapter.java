package com.coupledating.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coupledating.R;
import com.coupledating.models.ProfileData;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by anupamchugh on 26/12/15.
 */
public class ProfileAdapter extends PagerAdapter {
    // https://s-media-cache-ak0.pinimg.com/originals/6a/d5/a1/6ad5a1d491584482498d007cd0ee072d.jpg
    List<ProfileData> profileData;
    private Context mContext;

    public ProfileAdapter(Context context, List<ProfileData> profData) {
        mContext = context;
        this.profileData = profData;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        ProfileData modelObject = profileData.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.layout_profile, null);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescr = (TextView) view.findViewById(R.id.tvDescr);
        ImageView imgProfile = (ImageView) view.findViewById(R.id.imgProfile);

        tvTitle.setText(modelObject.getName());
        tvDescr.setText(modelObject.getDescription());

        if (!TextUtils.isEmpty(modelObject.getProfilePic()))
            Picasso.with(mContext).load(modelObject.getProfilePic()).placeholder(R.drawable.pic_loading)// Place holder image from drawable folder
                    .error(R.drawable.no_profile_pic)//.resize(110, 110).fit()
                    .into(imgProfile);

//        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
//        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return profileData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ProfileData customPagerEnum = profileData.get(position);
        return customPagerEnum.getName();
    }

}
