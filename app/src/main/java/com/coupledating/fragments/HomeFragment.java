package com.coupledating.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.coupledating.R;
import com.coupledating.activities.HomeActivity;
import com.coupledating.adapters.ProfileAdapter;
import com.coupledating.models.ProfileData;
import com.coupledating.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * CoupleDate
 * com.coupledating.fragments
 * Created by Srikanth Adapa on 8/30/2017 at 7:05 PM..
 * Copyrights @ BFMT
 */

public class HomeFragment extends Fragment {
    @Bind(R.id.vpgProfiles)
    ViewPager vpgProfiles;
    @Bind(R.id.btnPrev)
    ImageButton btnPrev;
    @Bind(R.id.btnNext)
    ImageButton btnNext;

    private List<ProfileData> profileData;
    private ProfileAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.nav_home, menu);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setTitle("Home");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.btnPrev, R.id.btnNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                vpgProfiles.setCurrentItem(vpgProfiles.getCurrentItem() - 1);
                break;
            case R.id.btnNext:
                vpgProfiles.setCurrentItem(vpgProfiles.getCurrentItem() + 1);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileData = new ArrayList<>();
        vpgProfiles = (ViewPager) view.findViewById(R.id.vpgProfiles);
        String str_data = CommonUtils.loadJSONFromAsset(getActivity(), "quiz.json");
        try {
            JSONObject jObj = new JSONObject(str_data);
            JSONArray jArray = jObj.getJSONArray("quiz");
            ProfileData mQuiz;
            for (int i = 0; i < jArray.length(); i++) {
                mQuiz = new ProfileData();
                mQuiz.setName(jArray.getJSONObject(i).getString("question"));
                mQuiz.setDescription(jArray.getJSONObject(i).getString("a1"));
                mQuiz.setProfilePic(jArray.getJSONObject(i).getString("a2"));
                profileData.add(mQuiz);
            }

            adapter = new ProfileAdapter(getActivity(), profileData);
            vpgProfiles.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        vpgProfiles.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                chengeBtns(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void chengeBtns(int position) {
        if (position == 0) {
            btnPrev.setVisibility(View.GONE);
        } else if (position == profileData.size() - 1) {
            btnNext.setVisibility(View.GONE);
        } else {
            btnPrev.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
        }
    }
}
