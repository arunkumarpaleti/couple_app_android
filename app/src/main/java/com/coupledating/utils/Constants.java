package com.coupledating.utils;

/**
 * COUPLEDATING
 * com.coupledating.utils
 * Created by Srikanth Adapa on 9/10/2017 at 10:58 PM..
 * Copyrights @ BFMT
 */
public class Constants {
    public static String BASE_URL = "http://www.21cssindia.com/couplemeetup/webservices/api.php/";
    public static String M_USERS = "users?";

    // https://stackoverflow.com/questions/31327897/custom-facebook-login-button-android
    // https://developers.facebook.com/docs/facebook-login/android
    public static String FB_APP_ID = "727011500817271";
}
