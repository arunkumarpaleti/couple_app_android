package com.coupledating.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.coupledating.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * COUPLEDATING
 * com.coupledating.utils
 * Created by Srikanth Adapa on 8/31/2017 at 5:49 PM..
 * Copyrights @ BFMT
 */

public class CommonUtils {
    public static String loadJSONFromAsset(Context mContext, String title) {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open(title);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
//        Log.e(TAG, "Json response " + json);
        return json;
    }

    public static void tst(Context mContext, String message) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.layout_toast, null);
        TextView text = (TextView) layout.findViewById(R.id.custom_txtToast);
        text.setText(message);
        Toast toast = new Toast(mContext);
        toast.setView(layout);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 100);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
//        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }
}
