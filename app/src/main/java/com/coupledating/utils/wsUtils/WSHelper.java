package com.coupledating.utils.wsUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.coupledating.application.MyApplication;

import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * COUPLEDATING
 * com.coupledating.utils.wsUtils
 * Created by Srikanth Adapa on 9/11/2017 at 11:02 PM..
 * Copyrights @ BFMT
 */
public class WSHelper {
    public synchronized static void makeStringReq(Context mContext, String url, final int requestCode, final IResponseCallBack listener, int method) {
        final ProgressDialog pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...");
        pDialog.show();

        StringRequest strReq = new StringRequest(method,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                listener.onSuccess(requestCode, response);
                pDialog.hide();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                listener.onError(requestCode, error.getMessage());
                pDialog.hide();
            }
        });

// Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, requestCode + "");
    }


    public synchronized static void StReqMap(Context mContext, String url, final int requestCode, final Map<String, String> params, final IResponseCallBack listener, int method) {
        final ProgressDialog pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...");
        pDialog.show();

        final StringRequest strReq = new StringRequest(method,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                listener.onSuccess(requestCode, response);
                pDialog.hide();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                listener.onError(requestCode, error.getMessage());
                pDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
// Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, requestCode + "");
    }
}
