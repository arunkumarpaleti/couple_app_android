package com.coupledating.models;

/**
 * COUPLEDATING
 * com.coupledating.models
 * Created by Srikanth Adapa on 8/31/2017 at 4:09 PM..
 * Copyrights @ BFMT
 */

public class ProfileData {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    private String name;
    private String description;
    private String profilePic;
}
