package com.coupledating.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.coupledating.R;
import com.coupledating.utils.CommonUtils;
import com.coupledating.utils.Constants;
import com.coupledating.utils.wsUtils.IResponseCallBack;
import com.coupledating.utils.wsUtils.WSHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.coupledating.R.id.etName;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etPwd)
    EditText etPwd;
    @Bind(R.id.tvSignUp)
    TextView tvSignUp;
    @Bind(R.id.login_button)
    LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);
        ButterKnife.bind(this);
        
        SpannableString ss = new SpannableString("Don't you have account? Signup");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                startActivity(new Intent(this, NextActivity.class));
                navigateActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };


        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.parseColor("#41CCE8"));
//        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(158, 158, 158));
        ss.setSpan(fcs, 23, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ss.setSpan(clickableSpan, 23, 30, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        tvSignUp.setText(ss);
        tvSignUp.setMovementMethod(LinkMovementMethod.getInstance());
        tvSignUp.setHighlightColor(Color.TRANSPARENT);

        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email");
        // If using in a fragment
//        loginButton.setFragment(this);
        // Other app specific specialization

        /*loginButton.setReadPermissions("user_friends");
        loginButton.setReadPermissions("public_profile");
        loginButton.setReadPermissions("email");
        loginButton.setReadPermissions("user_birthday");*/
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));

        final Bundle params = new Bundle();
        params.putString("fields", "id,email,gender,cover,picture.type(large)");

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("ACCESS TOKEN :", loginResult.getAccessToken() + "");

               /* new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                if (response != null) {
                                    try {
                                        JSONObject data = response.getJSONObject();
                                        if (data.has("picture")) {
                                            String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }).executeAsync();*/

                /*https://graph.facebook.com/113786359363336/picture?width=100&height=100*/

                /*{
                    "id": "113786359363336",
                        "name": "Kartheek Dvpt",
                        "email": "kartheekakula123@gmail.com",
                        "gender": "male",
                        "birthday": "05/10/1993"
                }*/

//                https://stackoverflow.com/questions/32310878/how-to-get-facebook-profile-image-in-android/
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity Response ", response.toString());
                                // Application code
                                try {
                                    String Name = object.getString("name");
                                    String email = object.getString("email");
                                    String birthday = object.getString("birthday"); // 01/31/1980 format
                                    String fbUserId = object.getString("id");

                                    Map<String, String> parameters = new HashMap<String, String>();
                                    parameters.put("user_fuid", fbUserId);
                                    parameters.put("user_ffname", Name);
                                    parameters.put("user_femail", email);
                                    parameters.put("user_profilepic", "");

                                    // http://www.21cssindia.com/couplemeetup/webservices/api.php/users?user_ffname=John&user_femail=john@gmail.com&user_profilepic=&user_createdon=1505153243501
                                    //http://www.21cssindia.com/couplemeetup/webservices/api.php/users?user_fuid=113786359363327&user_ffname=John&user_femail=john@gmail.com&user_profilepic=&user_createdon=1505153243501&user_modifiedon=1505153243501
                                    WSHelper.StReqMap(LoginActivity.this, Constants.BASE_URL + Constants.M_USERS, 1, parameters, new IResponseCallBack() {
                                        @Override
                                        public void onSuccess(int requestCode, String response) {
                                            CommonUtils.tst(LoginActivity.this, response);
                                            navigateActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                        }

                                        @Override
                                        public void onError(int requestCode, String error) {
                                            CommonUtils.tst(LoginActivity.this, error);
                                        }
                                    }, Request.Method.POST);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture"); //picture.type(large)
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("LOGIN RESULT :", "CANCELLED");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("LOGIN RESULT :", exception.getMessage());
            }
        });
    }

    public void onClickFB(View view) {
        loginButton.performClick();
    }

    public void onClickLogin(View view) {
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            CommonUtils.tst(this, "Please enter email address.");
        } else if (TextUtils.isEmpty(etPwd.getText().toString())) {
            CommonUtils.tst(this, "Please enter password.");
        } else {
            navigateActivity(new Intent(this, HomeActivity.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
