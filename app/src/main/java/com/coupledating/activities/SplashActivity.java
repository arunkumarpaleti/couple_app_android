package com.coupledating.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.coupledating.R;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        goNext();
    }

    public void goNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateActivity(new Intent(SplashActivity.this, WalkthroughActivity.class));
                finish();
            }
        }, 2000);
    }
}
