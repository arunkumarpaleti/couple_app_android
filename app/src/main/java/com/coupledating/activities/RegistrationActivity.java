package com.coupledating.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.coupledating.R;
import com.coupledating.utils.CommonUtils;
import com.coupledating.utils.Constants;
import com.coupledating.utils.wsUtils.IResponseCallBack;
import com.coupledating.utils.wsUtils.WSHelper;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends BaseActivity {

    @Bind(R.id.cbTerms)
    CheckBox cbTerms;
    @Bind(R.id.tvTerms)
    TextView tvTerms;
    @Bind(R.id.profilePic)
    CircularImageView profilePic;
    @Bind(R.id.btnProfilePic)
    Button btnProfilePic;
    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etLoc)
    EditText etLoc;
    @Bind(R.id.etPwd)
    EditText etPwd;
    @Bind(R.id.etConPwd)
    EditText etConPwd;
    @Bind(R.id.btnRegister)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_new);
        ButterKnife.bind(this);

        SpannableString ss = new SpannableString("By signing in you agree to our Terms and Conditions.");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                startActivity(new Intent(this, NextActivity.class));
                Toast.makeText(getApplicationContext(), "Terms of services Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };


        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.parseColor("#41CCE8"));
        ss.setSpan(fcs, 32, 51, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ss.setSpan(clickableSpan, 31, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        tvTerms.setText(ss);
        tvTerms.setMovementMethod(LinkMovementMethod.getInstance());
        tvTerms.setHighlightColor(Color.TRANSPARENT);


        ClickableSpan termsOfServicesClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Terms of services Clicked", Toast.LENGTH_SHORT).show();
            }
        };

        /*makeLinks(myTextView, new String[] { "Term of services", "Privacy Policy" }, new ClickableSpan[] {
                termsOfServicesClick, privacyPolicyClick
        });*/
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    @OnClick({R.id.btnProfilePic, R.id.btnRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnProfilePic:
                break;
            case R.id.btnRegister:
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("user_fuid", "113786359363327");
                parameters.put("user_ffname", etName.getText().toString());
                parameters.put("user_femail", etEmail.getText().toString());
                parameters.put("user_profilepic", "");

                // http://www.21cssindia.com/couplemeetup/webservices/api.php/users?user_ffname=John&user_femail=john@gmail.com&user_profilepic=&user_createdon=1505153243501
                //http://www.21cssindia.com/couplemeetup/webservices/api.php/users?user_fuid=113786359363327&user_ffname=John&user_femail=john@gmail.com&user_profilepic=&user_createdon=1505153243501&user_modifiedon=1505153243501
                WSHelper.StReqMap(RegistrationActivity.this, Constants.BASE_URL + Constants.M_USERS, 1, parameters, new IResponseCallBack() {
                    @Override
                    public void onSuccess(int requestCode, String response) {
                        CommonUtils.tst(RegistrationActivity.this, response);
                    }

                    @Override
                    public void onError(int requestCode, String error) {
                        CommonUtils.tst(RegistrationActivity.this, error);
                    }
                }, Request.Method.POST);
                break;
        }
    }
}
